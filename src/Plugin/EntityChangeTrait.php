<?php
/**
 * Created by PhpStorm.
 * User: steve
 * Date: 05/09/18
 * Time: 08:44
 */

namespace Drupal\entity_change\Plugin;

use Drupal\Core\Entity\EntityInterface;

/**
 * Trait EntityChangeTrait
 *
 * Every EntityChange plugin needs to check whether the plugin type
 * applies to the incoming entity. This handles that issue generically.
 *
 * For special situations the plugin does not need to use this trait.
 *
 * @package Drupal\entity_change\Plugin
 */
trait EntityChangeTrait {

  /**
   * Determine if the given entity matches the type for this plugin.
   *
   * @return bool
   */
  public function applies() {
    $applies = TRUE;

    // Do we have a specified type? (If not, or if it's match everything skip)
    if (($type = $this->pluginDefinition['type']) || $type == '*:*') {

      // Okay, we have a specified type. Start with "not applies".
      $applies = FALSE;

      list($pType, $pBundle, ) = explode(':', $type . ':');
      $pBundle = $pBundle ?: '*';

      /** @var EntityInterface $entity */
      $entity = $this->getContextValue('entity');

      if ($pType === '*' || $entity->getEntityTypeId() === $pType) {
        if ($pBundle === '*' || $entity->bundle() === $pBundle) {
          $applies = TRUE;
        }
      }
    }

    return $applies;
  }

}
