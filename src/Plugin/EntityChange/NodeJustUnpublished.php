<?php
/**
 * Created by PhpStorm.
 * User: steve
 * Date: 02/08/18
 * Time: 14:13
 */

namespace Drupal\entity_change\Plugin\EntityChange;

use Drupal\Core\Annotation\ContextDefinition;
use Drupal\Core\Annotation\Translation;
use Drupal\Core\Entity\Entity;
use Drupal\entity_change\Annotation\EntityChange;
use Drupal\entity_change\Plugin\EntityChangeBase;
use Drupal\entity_change\Plugin\EntityChangeTrait;
use Drupal\node\Entity\Node;

/**
 * Provides a 'Node Just Unpublished' entity change test.
 *
 * This checks if a node was just unpublished, not if it was always unpublished.
 *
 * @EntityChange(
 *   id = "node_just_unpublished",
 *   label = @Translation("Node Just Unpublished"),
 *   context = {
 *     "entity" = @ContextDefinition("entity:node", required = true, label = @Translation("Updated")),
 *     "original" = @ContextDefinition("entity:node", required = true, label = @Translation("Original"))
 *   },
 *   type = "node:*"
 * )
 */
class NodeJustUnpublished extends EntityChangeBase {

  use EntityChangeTrait;

  /**
   * Perform the plugin-specific check.
   *
   * @param Entity $new
   * @param Entity $old
   *
   * @return bool
   */
  protected function hasChanged($new, $old) {
    /**
     * @var Node $new
     * @var Node $old
     */
    return $old->isPublished() && !$new->isPublished();
  }
}
