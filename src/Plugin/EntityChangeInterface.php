<?php

namespace Drupal\entity_change\Plugin;

use Drupal\Component\Plugin\PluginInspectionInterface;

/**
 * Defines an interface for Entity change plugins.
 */
interface EntityChangeInterface extends PluginInspectionInterface {

}
