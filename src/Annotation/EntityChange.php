<?php

namespace Drupal\entity_change\Annotation;

use Drupal\Component\Annotation\Plugin;

/**
 * Defines a Entity change item annotation object.
 *
 * @see \Drupal\entity_change\Plugin\EntityChangeManager
 * @see plugin_api
 *
 * @Annotation
 */
class EntityChange extends Plugin {


  /**
   * The EntityChange plugin ID.
   *
   * @var string
   */
  public $id;

  /**
   * The human-readable name of the EntityChange plugin.
   *
   * @var \Drupal\Core\Annotation\Translation
   *
   * @ingroup plugin_translatable
   */
  public $label;

  /**
   * The name of the module providing the EntityChange plugin.
   *
   * @var string
   */
  public $module;

  /**
   * An array of context definitions describing the context used by the plugin.
   *
   * The array is keyed by context names.
   *
   * @var \Drupal\Core\Annotation\ContextDefinition[]
   */
  public $context = [];

  /**
   * Optionally, identify the entity and/or bundle to which this applies.
   *
   * @var string
   */
  public $type = '';

}
