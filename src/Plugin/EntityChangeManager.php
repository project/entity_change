<?php

namespace Drupal\entity_change\Plugin;

use Drupal\Component\Plugin\Exception\ContextException;
use Drupal\Component\Plugin\Exception\PluginException;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityStorageException;
use Drupal\Core\Logger\LoggerChannelTrait;
use Drupal\Core\Plugin\DefaultPluginManager;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;

/**
 * Provides the Entity change plugin manager.
 */
class EntityChangeManager extends DefaultPluginManager {

  use StringTranslationTrait;
  use LoggerChannelTrait;

  /**
   * Constructs a new EntityChangeManager object.
   *
   * @param \Traversable $namespaces
   *   An object that implements \Traversable which contains the root paths
   *   keyed by the corresponding namespace to look for plugin implementations.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache_backend
   *   Cache backend instance to use.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler to invoke the alter hook with.
   */
  public function __construct(\Traversable $namespaces, CacheBackendInterface $cache_backend, ModuleHandlerInterface $module_handler) {
    parent::__construct('Plugin/EntityChange', $namespaces, $module_handler, 'Drupal\entity_change\Plugin\EntityChangeInterface', 'Drupal\entity_change\Annotation\EntityChange');

    $this->alterInfo('entity_change_entity_change_plugin_info');
    $this->setCacheBackend($cache_backend, 'entity_change_entity_change_plugin_plugins');
  }

  /**
   * Given the supplied entity, find all matching EntityChange plugins
   * and instantiate each one for this $entity and return them
   *
   * @param EntityInterface $entity
   *
   * @param EntityInterface $original
   *
   * @return EntityChangeBase[]
   */
  public function buildInstances(EntityInterface $entity, EntityInterface $original) {
    $logger = $this->getLogger('entity_change:manager');
    /** @var EntityChangeBase[] $entityChanges */
    $entityChanges = [];

    foreach ($this->getDefinitions() as $key => $definition) {
      try {
        /** @var EntityChangeBase $entityChange */
        $entityChange = $this->createInstance($key, ['context' => ['entity' => $entity, 'original' => $original]]);
        if ($entityChange->applies()) {
          // If it does apply, keep it.
          $entityChanges[$key] = $entityChange;
        }
      }
      catch (EntityStorageException $e) {
        $logger->error($this->t('Unable to instantiate EntityChange @k: @x.', ['@k' => $key, '@x' => $e->getMessage()]));
      }
      catch (ContextException $e) {
        $logger->error($this->t('Unable to instantiate EntityChange @k: @x.', ['@k' => $key, '@x' => $e->getMessage()]));
      }
      catch (PluginException $e) {
        $logger->error($this->t('Unable to instantiate EntityChange @k: @x.', ['@k' => $key, '@x' => $e->getMessage()]));
      }
    }


    return $entityChanges;
  }

}
