<?php

namespace Drupal\entity_change\Plugin;

use Drupal\Component\Plugin\Exception\PluginException;
use Drupal\Core\Entity\Entity;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\Logger\LoggerChannelInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Plugin\ContextAwarePluginBase;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Base class for Entity change plugins.
 */
abstract class EntityChangeBase extends ContextAwarePluginBase implements EntityChangeInterface, ContainerFactoryPluginInterface {

  use StringTranslationTrait;

  /**
   * @var LoggerChannelInterface
   */
  protected $logger;

  /**
   * Constructs a EntityChange object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param LoggerChannelFactoryInterface $loggerChannelFactory
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition,
                              LoggerChannelFactoryInterface $loggerChannelFactory) {

    parent::__construct($configuration, $plugin_id, $plugin_definition);

    $this->logger = $loggerChannelFactory->get('entity_change:' . $this->getPluginId());

    // Fix this problem with contexts.
    if (empty($this->context) && !empty($this->contexts)) {
      $this->context = $this->contexts;
    }
  }

  /**
   * @param ContainerInterface $container
   * @param mixed[] $configuration
   * @param string $plugin_id
   * @param mixed[] $plugin_definition
   *
   * @return EntityChangeInterface
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('logger.factory')
    );
  }

  //---------------------------------------------------------------------------

  /**
   * We use this to store the value so we only have to test once.
   *
   * @var bool
   */
  protected $hasChanged = NULL;

  /**
   * Is this EntityChange plugin appropriate to the supplied context?
   *
   * @return bool
   */
  abstract public function applies();

  /**
   * @return bool | null
   */
  final public function __invoke() {
    if ($this->hasChanged === NULL) {
      try {
        $new = $this->getContextValue('entity');
        $old = $this->getContextValue('original');
        $this->hasChanged = (bool) $this->hasChanged($new, $old);
      }
      catch (PluginException $e) {
        $this->logger->error($this->t('Could not get context values: %x', ['%x' => $e->getMessage()]));
      }
    }
    return $this->hasChanged;
  }

  /**
   * Perform the plugin-specific check.
   *
   * @param Entity $new
   * @param Entity $old
   *
   * @return bool
   */
  abstract protected function hasChanged($new, $old);

}
