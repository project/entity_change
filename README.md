Entity Change
=============
This module provides the plugin framework to make arbitrary checks on updated
content entities (node, term, user etc) to see if they have been changed.

See the node change plugins provided as examples.

The module entities only work on content entities, because they have an
"original" entity attached when they go through the entity update hooks, this
allows us to compare fields in an arbitrary way.

Plugins should specify precisely which entities they deal with.

This module does nothing on its own.
